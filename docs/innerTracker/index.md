# Inner Tracker overview

1. [Introduction](Intro.md)
2. [FC7 Setup](FC7setup.md)
3. [Firmware Setup](FWsetup.md)
4. [Software Setup](SWsetup.md)
5. [FPGA Configuration](FPGAcfg.md)
6. [Running Ph2_ACF](Running.md)
9. [Module Testing](ModuleTesting.md)
7. [Configuration File](ConfigFile.md)
8. [Voltage Settings](VoltageSettings.md)
10. [Monitoring Configuraion](Monitoring.md)
11. [Optical Readout Configuration](OpticalReadout.md)

For calibrations, see [IT Calibrations](calibrations/index.md).